import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ShareAngularElementsComponent } from './share-angular-elements/share-angular-elements.component';
import { LazyElementsModule } from '@angular-extensions/elements';
import { RedirectComponent } from './redirect/redirect.component';
import { SecondComponent } from './second/second.component';

@NgModule({
  declarations: [
    AppComponent,
    ShareAngularElementsComponent,
    RedirectComponent,
    SecondComponent,
 //   OtherElementComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LazyElementsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AppModule { }

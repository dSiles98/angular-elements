import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShareAngularElementsComponent } from './share-angular-elements.component';

describe('ShareAngularElementsComponent', () => {
  let component: ShareAngularElementsComponent;
  let fixture: ComponentFixture<ShareAngularElementsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShareAngularElementsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShareAngularElementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

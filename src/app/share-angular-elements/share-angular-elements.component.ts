import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-share-angular-elements',
  templateUrl: './share-angular-elements.component.html',
  styleUrls: ['./share-angular-elements.component.scss']
})
export class ShareAngularElementsComponent implements OnInit {
  url = '../../assets/new-ng-elements/new-ng-elements.js';

  constructor() { }

  ngOnInit(): void {
  }

  showLikes(likes: number) {
    console.log('likes:', likes);
  }
}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-second',
  templateUrl: './second.component.html',
  styleUrls: ['./second.component.scss']
})
export class SecondComponent implements OnInit {
  url = '../../assets/ng-angular-elements/ng-angular-elements.js';
  message = '';

  constructor() { }

  ngOnInit(): void {
  }

  showMessage(message: string) {
    this.message = message;
  }

}

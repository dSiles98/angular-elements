import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShareAngularElementsComponent } from './share-angular-elements/share-angular-elements.component';
import { RedirectComponent } from './redirect/redirect.component';
import { SecondComponent } from './second/second.component';

const routes: Routes = [
  { path: 'share', component: ShareAngularElementsComponent },
  { path: 'aqui', component: SecondComponent },
  { path: '**', component: RedirectComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

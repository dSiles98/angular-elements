import { DoBootstrap, Injector, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { createCustomElement } from '@angular/elements';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { NewButtonComponent } from './new-button/new-button.component';

@NgModule({
  declarations: [
    NewButtonComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    NoopAnimationsModule
  ],
  providers: []
})
export class AppModule implements DoBootstrap {
  constructor(injector: Injector) {
    const webComponentButton1 = createCustomElement(NewButtonComponent, { injector });
    customElements.define('second-vce-button', webComponentButton1);
  }

  ngDoBootstrap() {}
}

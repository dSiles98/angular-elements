import { DoBootstrap, Injector, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { createCustomElement } from '@angular/elements';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';

import { NgButtonSecondaryComponent } from './ng-button-secondary/ng-button-secondary.component';
import { NgCardComponent } from './ng-card/ng-card.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    NgButtonSecondaryComponent,
    NgCardComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    MatButtonModule,
    MatCardModule
  ],
  providers: [],
})
export class AppModule implements DoBootstrap {
  constructor(injector: Injector) {
    const webComponentButton = createCustomElement(NgButtonSecondaryComponent, { injector });
    customElements.define('ng-button-secondary', webComponentButton);

    const webComponentCard = createCustomElement(NgCardComponent, { injector });
    customElements.define('ng-card', webComponentCard);
  }

  ngDoBootstrap() {}
}

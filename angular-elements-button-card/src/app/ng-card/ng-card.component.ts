import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-ng-card',
  templateUrl: './ng-card.component.html',
  styleUrls: ['./ng-card.component.scss']
})
export class NgCardComponent implements OnInit {
  @Input() title: string = '';
  @Input() subTitle = '';
  @Input() image = '';
  @Input() content = '';
  @Input() counterLikes = 0;
  @Output() updateLikes = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {
  }

  addLike() {
    this.counterLikes += 1;
    this.updateLikes.emit(this.counterLikes);
  }

}

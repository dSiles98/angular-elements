import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-button-secondary',
  templateUrl: './ng-button-secondary.component.html',
  styleUrls: ['./ng-button-secondary.component.scss']
})
export class NgButtonSecondaryComponent implements OnInit {
  ngOnInit(): void { }

  @Input()
  label = 'Click me!';

  openAlert() {
    alert(`Open ${this.label}`);
  }
}
